<?php

namespace Mobilerider\MrSiteUsers\Tests\config\database\factories;


use Illuminate\Database\Eloquent\Factories\Factory;
use Mobilerider\MrSiteUsers\Tests\config\Models\User;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    public function visible(){

    }
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
             $this->faker->name(),
             $this->faker->unique()->safeEmail(),
             $this->faker->password(), // password
        ];
    }

}
