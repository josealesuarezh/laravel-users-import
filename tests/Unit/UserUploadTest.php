<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mobilerider\MrSiteUsers\app\Facades\DataHandler;
use Mobilerider\MrSiteUsers\Tests\BaseTest;


class UserUploadTest extends BaseTest
{
    use DatabaseMigrations, RefreshDatabase;

    private $faker;

    private $keys = [
        'name',
        'email',
        'password'
    ];

    private $validUsersData = [
        ["Ms. Gerda Hand Jr.", "glenda86@wilkinson.net", "XfzRax-zU|il"],
        ["Hollis Roob", "josiane26@larkin.com", "01?RD<"],
        ["Romaine Tillman", "zackary35@vandervort.com", "8LNsF236JHBV"],
        ["Ellis Deckow", "beahan.stephon@yahoo.com", "?I)Gqmn@bm"]
    ];


    public function testValidUserDataImport()
    {
        $this->importDocument($this->validUsersData,$this->keys);
        foreach ($this->validUsersData as $user) {
            $this->assertDatabaseHas('users', [
                "name" => $user[0],
                "email" => $user[1],
            ]);
            $this->assertCredentials([
                "email" => $user[1],
                "password" => $user[2]
            ]);
       }
    }

    public function testEmailAndPasswordFieldWithSpaces(){
        $spacedEmailPassword = [["Ms. Gerda Hand Jr.", "  glenda86@wilkinson.net ", " XfzRax-zU|il "]];
        $this->importDocument($spacedEmailPassword,$this->keys);
        $this->assertDatabaseHas('users', [
            "name" => "Ms. Gerda Hand Jr.",
            "email" => "glenda86@wilkinson.net",
        ]);
        $this->assertCredentials([
            "email" => "glenda86@wilkinson.net",
            "password" => "XfzRax-zU|il"
        ]);
    }

    public function testInvalidEmailAddress(){
        $spacedEmailPassword = [["Romaine Tillman", "@zackary35@vandervort.com@", "8LNsF236JHBV"]];
        $this->importDocument($spacedEmailPassword,$this->keys);
        $this->assertDatabaseHas('user_import_errors', [
            "user->name" => "Romaine Tillman",
            "user->email" => "@zackary35@vandervort.com@",
            "error_message" => "Invalid email address"
        ]);
    }

    public function testUpdateUserData(){
        $firstData = [
            ["Romaine Tillman", "@zackary35@vandervort.com@", "8LNsF236JHBV"], // email with error
            ["Ms. Gerda Hand Jr.", "glenda86@wilkinson.net", "XfzRax-zU|il"]
        ];
        $this->importDocument($firstData,$this->keys);

        $updatedData = [
            ["Romaine Tillman", "zackary35@vandervort.com", "8LNsF236JHBV"], //fixed error on email
            ["Ms. Gerda Hand Jr.", "glenda86@wilkinson.net", "UpdatedPassword"] //changed password
        ];
        $this->updateDocument($updatedData,$this->keys);

        //check if the user that trigger an error on the insert was added
        $this->assertDatabaseHas('users', [
            "name" => "Romaine Tillman",
            "email" => "zackary35@vandervort.com",
        ]);

        //check if the user password was updated
        $this->assertCredentials([
            "email" => "glenda86@wilkinson.net",
            "password" => "UpdatedPassword"
        ]);
    }

    private function importDocument($data,$keys)
    {
        DataHandler::temporalStorageDocumentData("Test import document", $data);
        DataHandler::handleUserImport($keys);
    }

    private function updateDocument($data,$keys)
    {
        DataHandler::temporalStorageDocumentData("Test update document", $data);
        DataHandler::handleUserUpdate($keys);
    }

}
