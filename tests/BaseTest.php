<?php

namespace Mobilerider\MrSiteUsers\Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Mobilerider\MrSiteUsers\app\Helpers\DataHandler;
use Orchestra\Testbench\TestCase;

abstract class BaseTest extends TestCase
{

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        Config::set('mrusers', $this->setConfig());
        Config::set('auth.providers.users.model',
            \Mobilerider\MrSiteUsers\Tests\Unit\Models\User::class
        );
        $this->loadMigrationsFrom(__DIR__.'/config/database/migrations');
        $this->app->bind('import-helper', DataHandler::class);
    }


    private function setConfig(){
        return [
            'user_rules' => [
                'email' => 'bail|email|required'
            ],
            'user_error_message' =>[
                'email.email' => 'Invalid email address'
            ],
            'ignore_emails' => [
                'dev@mobilerider.com',
                'admin@mobilerider.com'
            ],
            'import_ignore' => [
                'id',
                'created_at',
                'updated_at',
                'remember_token',
                'email_verified_at'
            ],
        ];
    }
}
