## Laravel users import

This ***Laravel*** plugin for Backpack, will make simpler the users import process from csv or Excel sheets. It will allow you
to import documents from 1 to n columns (_be aware that the document needs to contain a column with valid data 
for each of the imported model table required fields_). The package will create a Backpack crud table 
for the model on `config('auth.providers.users.model')` usually `User Model` adding the `Backpack\CRUD\app\Models\Traits\CrudTrait`
to the model to create a custom crud view for it. It will also add the `import from file` button on the top of the table in the crud view.


# Install
1) Requirements
    ```json
   {
       "repositories": {
            "type": "vcs",
            "no-api": true,
            "url": "https://gitlab+deploy-token-{ask an admin for the token}@gitlab.com/mobilerider/site-users-pkg.git"
        },      
       "require": {
            "backpack/crud": "4.1.*",
            "maatwebsite/excel": "^3.1"
        }
   }
   ```

2) In your terminal run:<br>
    ```
    composer require mobilerider/mr-site-users
    php artisan mrusers:install
    ```
3) To run it on local environment you must set in your local env `QUEUE_CONNECTION=database` and then run this commands to create the jobs table and handle 
   the job queue. [Read More about this step..](https://laravel.com/docs/8.x/queues#driver-prerequisites)
   ```
    php artisan queue:table
    php artisan migrate
    php artisan queue:work
    ```
   
4) For run the jobs queue on production using Amazon SQS you will need to set these variables on your environment.
    ```
    AWS_ACCESS_KEY_ID=,
    AWS_SECRET_ACCESS_KEY=
    SQS_PREFIX=
    SQS_QUEUE=
    SQS_SUFFIX=
    AWS_DEFAULT_REGION=
    ```
    * [Laravel Vapor](https://github.com/laravel/vapor-core) usually handles this step for you. 
#  Publish the config file
`php artisan mrusers:publish`

## Customize the published config file
1) Add the validation rules `user_rules` to be applied to the corresponding document cells .
2) Customize on `user_error_message` the default laravel error message for each validation rule.
3) Add to `import_ignore` the user table fields that are not going to be shown on the document mapping
```php
return [
    'user_rules' => [
        'email' => 'bail|email|required'
    ],
    
    'user_error_message' =>[
        'email.email' => 'Invalid email address'
    ],
    
    'import_ignore' => [
        'id',
        'created_at',
        'updated_at',
        'remember_token',
        'email_verified_at'
    ],
];
```

## Usage
1) Go to `/admin/user` route to see the import view.
2) Click the `Browse` button and select the csv or Excel file you want to import.
3) Click the `Add from:` button, that will bring up a modal.
4) Select the database field corresponding to each document column on the selects fields in the modal.
5) Select the `Import Data` option to add a fresh document, or the update options to update 
   the existing data with de document content. 
* Note: Right now there is nothing that let you know when the import job is finish

## Test
Run tests
```
php vendor/bin/phpunit 
```

## Credits
- [Jose Alejandro](https://github.com/josealesuarezh)

## Security
If you discover any security-related issues, please email josealesuarezh@gmail.com instead of using the issue tracker.

## License
The MIT License (MIT). Please see [License File](/LICENSE.md) for more information.
