<?php

namespace Mobilerider\MrSiteUsers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Mobilerider\MrSiteUsers\app\Console\Commands\AddCrudTraitCommand;
use Mobilerider\MrSiteUsers\app\Console\Commands\InstallCommand;
use Mobilerider\MrSiteUsers\app\Console\Commands\PublishResources;
use Mobilerider\MrSiteUsers\app\Helpers\DataHandler;
use Mobilerider\MrSiteUsers\app\View\Components\SideMenu;


class MrSiteUsersServiceProvider extends ServiceProvider
{

    public $routeFilePath = '/routes/custom.php';

    /**
     * Register services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('import-helper', DataHandler::class);
        $this->commands([
            AddCrudTraitCommand::class,
            InstallCommand::class,
            PublishResources::class,
        ]);
    }
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setupRoutes();
        $this->mergeConfigFrom(__DIR__ . '/config/mrusers.php', 'mrusers');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'mobilerider.MrSiteUsers');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->publishes([
            __DIR__ . 'config/mrusers.php' => config_path('mrusers.php')
        ], 'config');
        Blade::component('upload-users-menu', SideMenu::class);
    }

    public function setUpRoutes()
    {
        $path = __DIR__ . $this->routeFilePath;
        $this->loadRoutesFrom($path);
    }

    public static function setupImportConfig(){
        if (!config('mobilerider.import_fields')){
            $model = config('auth.providers.users.model');
            \config(['mobilerider.import_fields' => array_values(
                array_diff(
                    Schema::getColumnListing(app($model)->getTable() ?? 'users'),
                    config('mrusers.import_ignore')
                )
            )]);
        }
    }
}
