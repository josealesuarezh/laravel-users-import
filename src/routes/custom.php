<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

use Mobilerider\MrSiteUsers\app\Http\Controllers\ImportController;

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'Mobilerider\MrSiteUsers\app\Http\Controllers',
], function () { // custom admin routes
    Route::crud('import-errors', 'ImportErrorsCrudController');
    Route::crud('user-import', 'UserCrudController');
    Route::post('user/parseExcel', [ImportController::class,'parseExcel']);
    Route::post('user/importExcel', [ImportController::class,'importExcel']);
    Route::post('user/updateUsers', [ImportController::class,'updateUsers']);
}); // this should be the absolute last line of this file
