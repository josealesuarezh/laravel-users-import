<?php

namespace Mobilerider\MrSiteUsers\app\Facades;

/**
 * Data Handler Facade
 *
 * @method static void handleUserImport(array $keys)
 * @method static void handleUserUpdate(array $keys)
 *  @method static void temporalStorageDocumentData($document_name,$data)
 */
class DataHandler extends \Illuminate\Support\Facades\Facade
{
    public static function getFacadeAccessor()
    {
        return 'import-helper';
    }
}
