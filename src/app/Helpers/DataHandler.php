<?php

namespace Mobilerider\MrSiteUsers\app\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Mobilerider\MrSiteUsers\app\Models\ImportErrors;

class DataHandler
{
    public function handleUserImport($keys)
    {
        $userModel = config('auth.providers.users.model');
        $encodedData = DB::table('temp_data')->first();
        $this->dataParser($keys, $encodedData, function ($userData) use ($userModel) {
            $userModel::insert($userData);
        });
    }

    public function handleUserUpdate($keys)
    {
        $userModel = config('auth.providers.users.model');
        $encodedData = DB::table('temp_data')->first();
        $this->dataParser($keys, $encodedData, function ($userData) use ($userModel) {
            $email = $userData['email'];
            unset($userData["email"]);
            $userModel::updateOrCreate(
                ["email" => $email],
                $userData
            );
        });
    }

    public function temporalStorageDocumentData($document_name,$data){
        DB::table('temp_data')->truncate();
        DB::table('temp_data')->insert([
            'file_name' => $document_name . '-' . Str::random(4),
            'row_count' => count($data),
            'temp_data' => json_encode($data)
        ]);
    }

    public static function dataParser($keys, $encodedData, $callbackFunction)
    {
        $tempData = json_decode($encodedData->temp_data);
        $fileName = $encodedData->file_name;
        $validationRules = config('mrusers.user_rules');
        $validationMessage = config('mrusers.user_error_message');

        foreach ($tempData as $user) {
            $rawPeer = array_combine($keys, (array)$user);
            unset($rawPeer["null"]);
            $userData = array_map('trim', $rawPeer);

            if (array_key_exists('password', $userData)) {
                $userData["password"] = Hash::make($userData["password"]);
            }
            try {
                $validator = Validator::make($userData, $validationRules, $validationMessage);
                $validator->validate();
                $callbackFunction($userData);
            } catch (\Exception $exception) {
                if ($exception instanceof ValidationException) {
                    $message = $validator->errors()->all()[0];
                } else {
                    $message = $exception->getPrevious()->getMessage() ?? $exception->getMessage();
                }
                unset($userData["password"]);
                $errorRow = new ImportErrors([
                    'file_name' => $fileName,
                    'user' => json_encode($userData),
                    'error_message' => $message ?? json_encode($message)
                ]);
                $errorRow->saveQuietly();
            }
        }
    }
}
