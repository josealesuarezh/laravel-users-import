<?php

namespace Mobilerider\MrSiteUsers\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class InstallCommand extends Command
{
    use \Backpack\CRUD\app\Console\Commands\Traits\PrettyCommandOutput;

    protected $progressBar;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mrusers:install
                                        {--timeout=300} : How many seconds to allow each process to run.
                                        {--debug} : Show process output or not. Useful for debugging.';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install mr-site-user package';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->progressBar = $this->output->createProgressBar(2);
        $this->progressBar->minSecondsBetweenRedraws(0);
        $this->progressBar->maxSecondsBetweenRedraws(120);
        $this->progressBar->setRedrawFrequency(1);
        $this->info(' Mobilerider Site Checklist installation started. Please wait...');
        $this->progressBar->start();

        $this->line(' Adding use CrudTrait to user model');
        Artisan::call('mrusers:add_crud_trait');
        $this->progressBar->advance();

        $this->line(' Adding link to sidebar content');
        $this->addSideBarContent();

        $this->progressBar->finish();
        $this->info(' Mobilerider site users installation finished.');
        return 0;
    }

    public function addSideBarContent()
    {
        Artisan::call('backpack:add-sidebar-content', ['code' => '<x-upload-users-menu/>']);
    }

}
