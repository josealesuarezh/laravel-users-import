<?php

namespace Mobilerider\MrSiteUsers\app\Console\Commands;

use Illuminate\Console\Command;

class PublishResources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mrusers:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish the assets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->call('vendor:publish --tag=config');
        return 0;
    }
}
