<?php

namespace Mobilerider\MrSiteUsers\app\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportErrors extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;
    protected $table = 'user_import_errors';
    protected $fillable = [
        'file_name',
        'user',
        'error_message'
    ];
}
