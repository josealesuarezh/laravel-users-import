<?php

namespace Mobilerider\MrSiteUsers\app\Imports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class TempImport implements FromCollection,WithHeadingRow
{
    use Importable;

    public function collection(){
        return TempImport::all();
    }

}
