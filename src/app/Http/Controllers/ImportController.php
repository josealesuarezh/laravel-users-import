<?php


namespace Mobilerider\MrSiteUsers\app\Http\Controllers;


use App\Http\Controllers\Controller;

use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\HeadingRowImport;
use Mobilerider\MrSiteUsers\app\Facades\DataHandler;
use Mobilerider\MrSiteUsers\app\Imports\TempImport;
use Illuminate\Support\Facades\DB;
use Mobilerider\MrSiteUsers\app\Jobs\ImportUsersJob;
use Mobilerider\MrSiteUsers\app\Jobs\UpdateUsersJob;
use Prologue\Alerts\Facades\Alert;


class ImportController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = config('auth.providers.users.model');
    }

    public function importExcel(Request $request)
    {
        ImportUsersJob::dispatch($request->fields);
        Alert::add('success', "Starting import proccess")->flash();
        return back();
    }


    public function updateUsers(Request $request)
    {
        UpdateUsersJob::dispatch($request->fields);
        Alert::add('success', "Starting the update proccess")->flash();
        return back();
    }

    public function parseExcel(Request $request)
    {
        if ($request->hasFile('import_file')) {
            $document_name = str_replace('.' . $request->import_file->getClientOriginalExtension(), '',
                $request->import_file->getClientOriginalName());
            $headingRow = (new HeadingRowImport())->toArray(request()->file('import_file'))[0];
            $data = Excel::toCollection(new TempImport(), request()->file('import_file'))->collapse()->toArray();

            DataHandler::temporalStorageDocumentData($document_name,$data);
            $dataCount = count($data);
            $length = $dataCount <= 3 ? $dataCount : 4;
            $dataPreview = array_slice($data, 0, $length);
            if ($dataCount > 0) {
                session()->flash('import_data', array_merge($headingRow, $dataPreview));
            } else {
                Alert::add('error', "Excel file must have at least one valid row")->flash();
            }
        }
        return back();
    }
}
