<?php

namespace Mobilerider\MrSiteUsers\app\Http\Controllers;

use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Hash;
use Mobilerider\MrSiteUsers\app\Http\Requests\UserRequest;
use Mobilerider\MrSiteUsers\MrSiteUsersServiceProvider;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(config('auth.providers.users.model'));
        CRUD::setRoute(config('backpack.base.route_prefix') . '/user-import');
        CRUD::setEntityNameStrings('user', 'users');
        $this->crud->setListView('mobilerider.MrSiteUsers::crud.list');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addClause('whereNotIn', 'email', config('mrusers.ignore_emails'));
        $this->crud->enableExportButtons();
        $this->crud->addButton('top', 'Import', 'view','mobilerider.MrSiteUsers::crud.buttons.import', 'end');
        $this->crud->setLabeller(function ($value) {
            return ucwords(str_replace("id", "ID", str_replace("_", " ", $value)));
        })->setFromDB();
        $this->crud->removeColumn('password');
        $this->crud->removeColumn('remember_token');
        $this->crud->removeColumn('email_verified_at');
        MrSiteUsersServiceProvider::setupImportConfig();
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(UserRequest::class);


        $this->crud->setLabeller(function ($value) {
            return ucwords(str_replace("id", "ID", str_replace("_", " ", $value)));
        })->setFromDB();
        $this->crud->removeField('remember_token');
        $this->crud->removeField('email_verified_at');



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $request = $this->crud->getRequest();
        $this->handlePasswordInput($request);
        // do something before validation, before save, before everything
        $response = $this->traitStore();
        // do something after save
        return $response;
    }
    public function update()
    {
        $this->crud->unsetValidation();
        $this->crud->setRequest($this->handlePasswordInput($this->crud->getRequest()));
        return $this->traitUpdate();
    }
    protected function handlePasswordInput($request)
    {
        $request->request->remove('password_confirmation');
        if ($request->input('password')) {
            $request->request->set('password', Hash::make($request->input('password')));
        } else {
            $request->request->remove('password');
        }
        return $request;
    }
}
