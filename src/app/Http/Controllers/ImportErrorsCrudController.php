<?php

namespace Mobilerider\MrSiteUsers\app\Http\Controllers;;

use Mobilerider\MrSiteUsers\app\Http\Requests\ImportErrorsRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Mobilerider\MrSiteUsers\app\Models\ImportErrors;


/**
 * Class ImportErrorsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ImportErrorsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->crud->enableExportButtons();
        CRUD::setModel(ImportErrors::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/import-errors');
        CRUD::setEntityNameStrings('import errors', 'import errors');
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('file_name');
        $this->crud->addColumn([
            'name'     => 'email',
            'label'    => 'User email',
            'type'     => 'closure',
            'function' => function($entry) {
                return json_decode($entry->user)->email;
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'error_message',
            'label'    => 'Error',
            'type'     => 'closure',
            'function' => function($entry) {
                if (str_contains($entry->error_message,'SQLSTATE[23505]'))
                    $error = "Duplicated Email";
                return $error ?? $entry->error_message;
            }
        ]);
        CRUD::column('error_message');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

}
