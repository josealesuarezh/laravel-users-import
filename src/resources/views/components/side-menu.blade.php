
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cloud-upload"></i> Upload</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href="{{ backpack_url('user-import') }}"><i class='nav-icon la la-user'></i> Users</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('import-errors') }}'><i class='nav-icon la la-exclamation-circle'></i> Errors Report</a></li>
    </ul>
</li>
