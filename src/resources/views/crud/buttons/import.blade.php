<div id="custom-container" style="padding-left: 0.5rem;">
    <form enctype="multipart/form-data" method="post" action="{{backpack_url('user/parseExcel')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="input-group " style="align-items: center;">
            <div class="input-group-prepend">
                <button class="btn btn-primary" type="submit" ><span class="la la-upload"></span>Add from:</button>
            </div>
            <div class="custom-file">
                <input type="file" name="import_file" id="my-custom-file" class="custom-file-input" onchange="setFileName(event)">
                <label class="custom-file-label" for="inputGroupFile03" style="display: inline-block; overflow: hidden;white-space: nowrap;">Choose file</label>
            </div>
        </div>
    </form>
</div>

@if(session()->has('import_data'))
    @include('mobilerider.MrSiteUsers::crud.modals.import_modal')
@endif
@push('after_scripts')
    <script type="application/javascript">
        function setFileName(e){
            var fileName = e.target.files[0].name;
            $('.custom-file-label').html(fileName);
        }
    </script>
@endpush
