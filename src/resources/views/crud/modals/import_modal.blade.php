@push('after_styles')
    <style>
        .remove_option{
            background-color: #f06767;
            color: white;
        }
    </style>
@endpush
<div class=" modal fade" tabindex="-1" id="importModal" style="z-index: 6000000!important;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Excel Import</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" method="post">
            <div class="modal-body" style="overflow: scroll;">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <table class="table text-center">
                        @foreach (session('import_data') as $row)
                            <tr>
                                @foreach ($row as $key => $value)
                                    <td>{{ $value }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                        <tr>
                            @foreach (session('import_data')[0] as $key => $value)
                                <td >
                                    <select  class="form-control" name="fields[{{ $key }}]" style="min-width: 95px; text-align: center;">
                                        <option disabled selected value="null">--Select--</option>
                                        @foreach (config('mobilerider.import_fields') as $index => $db_field)
                                            <option value="{{ $db_field }}">{{ $db_field }}</option>
                                        @endforeach
                                        <option onselect="" value="null" class="remove_option" >&cross; Ignore Column</option>
                                    </select>
                                </td>
                            @endforeach
                        </tr>
                    </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-secondary" formaction="{{backpack_url('user/updateUsers')}}">Update</button>
                <button type="submit" class="btn btn-primary" formaction="{{backpack_url('user/importExcel')}}">Import Data</button>
            </div>
            </form>
        </div>
    </div>
</div>

@push('after_scripts')
    <script type="text/javascript">
        $(document).ready(function (){
            $('#importModal').modal('show');
            $("#importModal").appendTo("body");
        });

    </script>
@endpush
