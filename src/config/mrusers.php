<?php
return [
    'user_rules' => [
        'email' => 'bail|email|required'
    ],
    'user_error_message' =>[
        'email.email' => 'Invalid email address'
    ],
    'ignore_emails' => [
        'dev@mobilerider.com',
        'admin@mobilerider.com'
    ],
    'import_ignore' => [
        'id',
        'created_at',
        'updated_at',
        'remember_token',
        'email_verified_at'
    ],
];
